<?php

require_once 'config/paths.php';
require_once 'config/database.php';

require_once 'function/sanitize.php';


set_include_path(
        get_include_path()
        .PATH_SEPARATOR."./classes/"
        .PATH_SEPARATOR."./libs/"
);

function __autoload($class_name) {
    try{
        require $class_name . '.php';
    }  catch (Exception $e){
        echo $e->getMessage();
    }
}




$app = new Bootstrap();

