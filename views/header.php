<html>
    <head>
        <title>MyRec</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css"/>
        <script type="text/javascript" src="<?php echo URL; ?>/public/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo URL; ?>/public/js/custom.js"></script>
        <?php
        if (isset($this->js)) {
            foreach ($this->js as $js) {
                echo '<script type="text/javascript" src="' . URL . 'views/' . $js . '"></script>';
            }
        }
        ?>
    </head>
    <body>
        <div id="header">
            <?php if ((Session::get('loggedIn') == true)): ?>   
                <a href="<?php echo URL; ?>dashboard" class="button">Booking</a>   
                <a href="<?php echo URL; ?>dashboard/reception" class="button">Reception desk</a>           
                <a href="<?php echo URL; ?>dashboard/logout" class="button">Log out</a> 
                <div id="config">
                    <a href="<?php echo URL; ?>config/index" class="button">Configuration</a> 
                </div>
            <?php else : ?>
                <a href="<?php echo URL; ?>index" class="button">Main</a>
            <?php endif; ?>
        </div>
        <div id="content">