$(function() {
    $.get('../config/roomsGetListings', function(o) {

        for (var i = 0; i < o.length; i++)
        {
            if ($('#roomsInsert').length == 1) {
                $('#listInserts').append('<div>' + o[i].type + ' ' + o[i].price + ' ' + o[i].numberOf + '<a class="del" rel="' + o[i].id + '" href="#"> X </a></div>');
            } else if ($('#mealsInsert').length == 1) {
                $('#listInserts').append('<div>' + o[i].type + ' ' + o[i].price + ' ' + o[i].halfPrice + '<a class="del" rel="' + o[i].id + '" href="#"> X </a></div>');
            } else {
                $('#listInserts').append('<div>' + o[i].type + ' ' + o[i].price + '<a class="del" rel="' + o[i].id + '" href="#"> X </a></div>');
            }
        }
        $('.del').live('click', function() {
            delItem = $(this);
            var id = $(this).attr('rel');

            $.post('../config/roomsDeleteListing', {'id': id}, function(o) {
                delItem.parent().remove();
            }, 'json');
            return false;
        });
    }, 'json');


    if ($('#roomsInsert').length == 1) {
        $('#roomsInsert').submit(function() {
            var url = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url, data, function(o) {
                $('#listInserts').append('<div>' + o.type + ' ' + o.price + ' ' + o.numberOf + '<a class="del" rel="' + o.id + '" href="#"> X </a></div>');
            }, 'json');
            return false;
        });
    } else if ($('#mealsInsert').length == 1) {
        $('#mealsInsert').submit(function() {
            var url = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url, data, function(o) {

                $('#listInserts').append('<div>' + o.type + ' ' + o.price + ' ' + o.halfPrice + '<a class="del" rel="' + o.id + '" href="#"> X </a></div>');
            }, 'json');
            return false;
        });
    } else {
        $('#insert').submit(function() {
            var url = $(this).attr('action');
            var data = $(this).serialize();
            $.post(url, data, function(o) {

                $('#listInserts').append('<div>' + o.type + ' ' + o.price + '<a class="del" rel="' + o.id + '" href="#"> X </a></div>');
            }, 'json');
            return false;
        });
    }
});
