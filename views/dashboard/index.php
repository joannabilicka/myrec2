<?php Session::set('config', false); ?>
<div id="title">BOOKING</div>
<form action="<?php echo URL ?>dashboard/start" method="post">
    <label>Firstname: </label><input type="text" name="firstname"/><br>
    <label>Lastname: </label><input type="text" name="lastname"/><br>
    <label>Phone number: </label><input type="tel" name="phone" maxlength="9"/><br>
    <label>Since: </label><input type="date" name="since" /><br>
    <label>Until: </label><input type="date" name="until"/><br>
    <label>House/room type: </label><select name="rtype" id="rtype" onchange="change(this.value)">   
        <option><?php echo '' ?></option>
        <?php 
        if (Session::get('loggedIn') == TRUE) {
            $login = Session::get('login');
            $this->login = $login;
        }
        $con = mysqli_connect("localhost", "root", "", "myrec");
        $sql = "SELECT type, numberOf FROM `" . $this->login . "_rooms`";
        $query = mysqli_query($con, $sql);
        while ($row = $query->fetch_assoc()):  ?>
        <option><?php echo $row['type']; ?></option>
        <?php endwhile; ?>   
    </select><br>
    <label>House/room number: </label><input type="number" name="rnumber" id="rnumber" min="1"/><br>
    <label></label><input type="submit" value="Insert" class="button"/>
    <br>
    
</form>     