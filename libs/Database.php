<?php

class Database extends PDO {

    public function __construct() {

        try {
            parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        } catch (PDOException $ex) {
            die($ex->getMessage());
        }
    }

    public function action($action, $table, $where = array(), $do = array()) {
        if (count($where) === 0) {
            $sth = $this->prepare("{$action} FROM {$table}");
            if (!$sth) {
                var_dump($this->errorInfo());
                die();
            }
            $sth->execute(array());
            return $sth;
        }
        if (count($where) === 3) {
            $operators = array('=', '>', '<', '>=', '<=');
            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];

            if (in_array($operator, $operators)) {
                $sth = $this->prepare("{$action} FROM {$table} WHERE {$field} {$operator} {$value}");
                if (!$sth) {
                    var_dump($this->errorInfo());
                    die();
                }
                if (empty($do)) {
                    $sth->execute();
                } else {
                    $sth->execute(array($value => $do));
                }
                return $sth;
            }
        }
        if (count($where) === 6) {
            $operators = array('=', '>', '<', '>=', '<=');
            $field1 = $where[0];
            $operator1 = $where[1];
            $value1 = $where[2];
            $field2 = $where[3];
            $operator2 = $where[4];
            $value2 = $where[5];
            $do1 = $do[0];
            $do2 = $do[1];

            if (in_array($operator1, $operators) && in_array($operator2, $operators)) {
                $sth = $this->prepare("{$action} FROM {$table} WHERE {$field1} {$operator1} {$value1} "
                        . "AND {$field2} {$operator2} {$value2}");
                if (!$sth) {
                    var_dump($this->errorInfo());
                    die();
                }
                if ( empty($do)) {
                    $sth->execute();
                } else {
                    $sth->execute(array(
                        $value1 => $do1,
                        $value2 => $do2
                    ));
                }
                return $sth;
            }
        }
        return FALSE;
    }

    public function select($table, $where, $do) {
        return $this->action('SELECT *', $table, $where, $do);
    }

    public function delete($table, $where, $do) {
        return $this->action('DELETE', $table, $where, $do);
    }

    public function insert($table, $fields = array()) {
        $keys = '';
        $values = '';
        $x = 1;
        foreach ($fields as $field) {
            $keys .= '`' . $field . '`';
            $values .=':' . $field;
            if ($x < count($fields)) {
                $keys.= ', ';
                $values.= ', ';
            }
            $x++;
        }
        $sth = $this->prepare("INSERT INTO {$table}({$keys}) VALUES ({$values})");
        if (!$sth) {
            var_dump($this->errorInfo());
            die();
        }
        return $sth;
    }

    public function create($table, $fields = array(), $primary) {
        $columns = '';
        foreach ($fields as $field) {
            $columns .= ' ' . $field . ', ';
        }
        $sth = $this->prepare("CREATE TABLE IF NOT EXISTS {$table} ({$columns} PRIMARY KEY ({$primary}))");
        if (!$sth) {
            var_dump($this->errorInfo());
            die();
        }
        $sth->execute();
    }

    public function add($table, $column) {
        $sth = $this->prepare("ALTER TABLE " . $table . " ADD " . $column . " INT (11)");
        if (!$sth) {
            var_dump($this->errorInfo());
            die();
        }
        $sth->execute();
    }

}
