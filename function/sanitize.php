<?php

function out($string){
    return htmlspecialchars($string);
}

function in($string){
    return strip_tags($string);
}