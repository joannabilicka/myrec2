<?php

class Guest {

    function __construct($firstnameFromMOdel, $lastnameFromModel, $phoneAgainFromModel) {
        $this->firstname = in($firstnameFromMOdel);
        $this->lastname = in($lastnameFromModel);
        $this->phone = in($phoneAgainFromModel);
    }
    
    public function getFirstName() {
        if (preg_match('/^[a-zA-Z]+$/D', $this->firstname)) {
            return $this->firstname;
        }
    }
    
    public function getLastName() {
        if (preg_match('/^[a-zA-Z]+$/D', $this->lastname)) {
            return $this->lastname;
        }
    }
    
    public function getPhoneNumber() {
        if (preg_match('/^[0-9]+$/D', $this->phone)) {
            return $this->phone;
        }
    }
    
    public function getCity() {
        if (preg_match('/^[a-zA-Z0-9\s\-]+$/D', $this->city)) {
            return $this->city;
        }
    }
    
    public function getStreet() {
        if (preg_match('/^[a-zA-Z0-9\s]+$/D', $this->street)) {
            return $this->street;
        }
    }
    
    public function getPESEL() {
        if (preg_match('/^[A-Z0-9]{11}$/D', $this->pesel)) {
            return $this->pesel;
        }
    }

}
