<?php

class Book{
    function __construct($idGuestFromModel, $idRoomFromModel, $sinceFromModel, $untilFromModel, $roomNumberFromModel) {
        $this->idGuest = in($idGuestFromModel);
        $this->idRoom = in($idRoomFromModel);
        $this->since = in($sinceFromModel);
        $this->until = in($untilFromModel);
        $this->roomNumber = in($roomNumberFromModel);
    }
    
    public function getIdGuest() {
        if (preg_match('/^[0-9]+$/D', $this->idGuest)) {
            return $this->idGuest;
        }
    }
    
    public function getIdRoom() {
        if (preg_match('/^[0-9]+$/D', $this->idRoom)) {
            return $this->idRoom;
        }
    }
    
    public function getRoomNumber() {
        if (preg_match('/^[0-9]+$/D', $this->roomNumber)) {
            return $this->roomNumber;
        }
    }
    
    public function getSince() {
        if (preg_match('/^([0-9]{4})-([1-9]{2})-([1-9]{2})$/D', $this->since)) {
            return $this->since;
        }
    }
    
    public function getUntil() {
        if (preg_match('/^([0-9]{4})-([1-9]{2})-([1-9]{2})$/D', $this->until)) {
            return $this->until;
        }
    }
}

