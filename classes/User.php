<?php

class User {

    function __construct($loginFromModel, $passwordFromModel, $passwordAgainFromModel, $emailFromModel) {
        $this->login = in($loginFromModel);
        $this->password = in($passwordFromModel);
        $this->passwordAgain = in($passwordAgainFromModel);
        $this->email = in($emailFromModel);
    }
    
    public function getLogin() {
        if (preg_match('/^[a-zA-Z0-9]+$/D', $this->login)) {
            return $this->login;
        }
    }

    public function getPassword() {
        if (preg_match('/^[a-zA-Z0-9]+$/D', $this->password)) {
            return sha1($this->password);
        }
    }
    
    public function getPasswordAgain() {
        if (preg_match('/^[a-zA-Z0-9]+$/D', $this->passwordAgain)) {
            return sha1($this->passwordAgain);
        }
    }
    
        public function getEmail() {
        if (preg_match('/^[a-zA-Z0-9\.\-_]+\@[a-zA-Z0-9\.\-_]+\.[a-z]{2,4}$/D', $this->email)) {
            return $this->email;
        }
    }
}
