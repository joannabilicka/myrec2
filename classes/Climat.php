<?php

class Climat {

    function __construct($typeFromModel, $priceFromModel) {
        $this->type = in($typeFromModel);
        $this->price = in($priceFromModel);
    }

    public function getType() {
        if (preg_match('/^[a-zA-Z0-9]+$/D', $this->type)) {
            return $this->type;
        }
    }

    public function getPrice() {
        if (preg_match('/^[0-9]+$/D', $this->price)) {
            return $this->price;
        }
    }
}
