<?php

class Room {

    function __construct($typeFromModel, $priceFromModel, $numberOfFromModel) {
        $this->type = in($typeFromModel);
        $this->price = in($priceFromModel);
        $this->numberOf = in($numberOfFromModel);
    }

    public function getType() {
        if (preg_match('/^[a-zA-Z0-9]+$/D', $this->type)) {
            return $this->type;
        }
    }

    public function getPrice() {
        if (preg_match('/^[0-9]+$/D', $this->price)) {
            return $this->price;
        }
    }

    public function getNumberOf() {
        if (preg_match('/^[0-9]+$/D', $this->numberOf)) {
            return $this->numberOf;
        }
    }

}
