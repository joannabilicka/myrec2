<?php

class Meal {

    function __construct($typeFromModel, $priceFromModel, $halfPriceFromModel) {
        $this->type = in($typeFromModel);
        $this->price = in($priceFromModel);
        $this->halfPrice = in($halfPriceFromModel);
    }

    public function getType() {
        if (preg_match('/^[a-zA-Z0-9]+$/D', $this->type)) {
            return $this->type;
        }
    }

    public function getPrice() {
        if (preg_match('/^[0-9]+$/D', $this->price)) {
            return $this->price;
        }
    }

    public function getHalfPrice() {
        if (preg_match('/^[0-9]+$/D', $this->halfPrice)) {
            return $this->halfPrice;
        }
    }

}
