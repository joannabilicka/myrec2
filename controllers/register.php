<?php

class Register extends Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->view->render('register/index');
    }

    function start() {
        
        $this->model->check();
        @$this->view->msg=$this->model->msg;
        @$this->view->login = $this->model->login;
        @$this->view->email=  $this->model->email;
        if ($this->model->msg != '') {
            $this->view->render('register/index');
        }
        
    }

}
