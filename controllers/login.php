<?php

class Login extends Controller{
    function __construct() {
        parent::__construct();        
    }
    
    function index(){
        $this->view->render('login/index');
    }
    
    function start(){
        $this->model->start();
        if(Session::get('loggedIn')== true){
            header('location:' . URL . 'dashboard');
        }else {
            header('location:' . URL . 'login');
        }
        echo 'login start';
    }
}