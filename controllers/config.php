<?php

class config extends Controller {

    function __construct() {
        parent::__construct();
        Session::init();
        Session::set('config', true);
        $this->view->js = array('config/js/default.js');
        
    }

    function index() { 
        $this->view->render('config/index');
        $this->model->table = Session::set('table', 'rooms');       
    }
    
    function meals(){
        $this->view->render('config/meals');
        $this->model->table = Session::set('table', 'meals');
    }    
    function climatic(){
        $this->view->render('config/climatic');
        $this->model->table = Session::set('table', 'climatic');
    }
        
    function other(){
        $this->view->render('config/other');
        $this->model->table = Session::set('table', 'other');
    }
             
    function save(){         
        $this->model->make_other_tables();
        $this->view->render('config/save'); 
    }

    function roomsInsert() {
        $this->model->roomsInsert();
    }

    function roomsGetListings() {
        $this->model->roomsGetListings();
    }

    function roomsDeleteListing() {
        $this->model->roomsDeleteListing();
    }
}

