<?php

class Dashboard extends Controller {

    function __construct() {
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedIn');
        if ($logged == false) {
            Session::destroy();
            header('location:' . URL . 'login');
            exit;
        }
        $this->view->js = array('dashboard/js/default.js');
    }

    function index() {
        $this->view->render('dashboard/index');
    }

    function logout() {
        Session::destroy();
        header('location: ../index');
        Session::set('config', false);
        exit;
    }

    function start() {
        $this->model->start();
        $this->view->msg = $this->model->msg;
        $this->view->render('dashboard/index');
        
    }

    function reception() {
        $this->model->reception();
    //    $this->view->msg = $this->model->msg;
        $this->view->render('dashboard/reception');
    }

}
