<?php

class Config_Model extends Model {

    function __construct() {
        parent::__construct();

        if (Session::get('reg') == TRUE) {
            Session::set('loggedIn', false);
            $login = Session::get('login');
            $table = Session::get('table');
            $this->login = $login;
            $this->table = $table;
        } else if (Session::get('loggedIn') == TRUE) {
            $login = Session::get('login');
            $table = Session::get('table');
            $this->login = $login;
            $this->table = $table;
        }
    }

    function roomsInsert() {
        $table = $this->login . "_" . $this->table;
        if ($this->table == 'rooms') {
            $room = new Room($_POST['type'], $_POST['price'], $_POST['numberOf']);
            $sth = $this->db->insert($table, array('type', 'price', 'numberOf'));
            $sth->execute(array(
                ':type' => $room->getType(),
                ':price' => $room->getPrice(),
                ':numberOf' => $room->getNumberOf()
            ));
            $data = array('type' => $room->getType(), 'price' => $room->getPrice(), 'numberOf' => $room->getNumberOf(), 'id' => $this->db->lastInsertId());
        } else if ($this->table == 'meals') {
            $meal = new Meal($_POST['type'], $_POST['price'], $_POST['halfPrice']);
            $sth = $this->db->insert($table, array('type', 'price', 'halfPrice'));
            $sth->execute(array(
                ':type' => $meal->type,
                ':price' => $meal->price,
                ':halfPrice' => $meal->halfPrice
            ));
            $data = array('type' => $meal->getType(), 'price' => $meal->getPrice(), 'halfPrice' => $meal->getHalfPrice(), 'id' => $this->db->lastInsertId());
        } else {
            $climat = new Climat($_POST['type'], $_POST['price']);
            $sth = $this->db->insert($table, array('type', 'price'));
            $sth->execute(array(
                ':type' => $climat->type,
                ':price' => $climat->price,
            ));
            $data = array('type' => $climat->getType(), 'price' => $climat->getPrice(), 'id' => $this->db->lastInsertId());
        }
        echo json_encode($data);
    }

    function roomsGetListings() {
        $table = $this->login . "_" . $this->table;
        $sth = $this->db->select($table, array(), array());
        $data = $sth->fetchAll();
        echo json_encode($data);
    }

    function roomsDeleteListing() {
        $id = $_POST['id'];
        $table = $this->login . "_" . $this->table;
        $sth = $this->db->delete($table, array('id', '=', $id), array());
    }

    function make_other_tables() {
        Session::set('regisered', TRUE);
        Session::set('config', false);
        Session::set('loggedIn', false);
        //create guests table for individual user
        @$sth = $this->db->create($this->login . '_guests', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'firstname VARCHAR (20) NOT NULL',
            'lastname VARCHAR (30) NOT NULL',
            'phoneNumber INT(9) NOT NULL',
            'city VARCHAR(20)',
            'street VARCHAR(20)',
            'PESEL INT(11)'), 'id');

        //create room/house booking table for individual user
        @$sth = $this->db->create($this->login . '_booking', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'id_guest INT(11) NOT NULL',
            'id_room INT(11) NOT NULL',
            'since DATE NOT NULL',
            'until DATE NOT NULL',
            'room_number INT (11)'), 'id');

        //create table for sumary meal price
        @$sth = $this->db->create($this->login . '_meals_price', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'id_booking INT(11) NOT NULL',
            'price INT(3) NOT NULL'), 'id');

        //create table for rabats
        @$sth = $this->db->create($this->login . '_rabat', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'id_booking INT(11) NOT NULL',
            'rabat INT(3)',
            'procent` INT (3)'), 'id');

        //create table for rooms charges
        @$sth = $this->db->create($this->login . '_room_charges', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'id_booking INT(11) NOT NULL',
            'id_rabat INT(11)',
            'total_due INT(11) NOT NULL',
            'expected_advance_payment INT (11) NOT NULL',
            'anted_advance_payment INT (11)',
            'remaining_due` INT (11)'), 'id');

        //create table for all charges
        @$sth = $this->db->create($this->login . '_all_charges', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'id_booking INT(11) NOT NULL',
            'id_rabat INT(11)',
            'id_meals_price INT (11)',
            'id_room_charges INT (11)',
            'id_climatic_payment INT (11)',
            'price INT (11)',
            'did BOOLEAN'), 'id');

        //create tables for meals.  
        @$sql = $this->db->select($this->login . '_meals', array(), array());
        while ($row = $sql->fetch()) {
            $table = $this->login . '_' . $row['type'];
            $sth = $this->db->create($table, array(
                'id INT(11) NOT NULL AUTO_INCREMENT',
                'id_booking INT(11) NOT NULL',
                'numebrOf INT(11) NOT NULL',
                'numberOfHalf INT (11)',
                'since DATE NOT NULL',
                'until DATE NOT NULL',
                'price INT(3) NOT NULL'), 'id');
        }

        //create table for number of people
        @$table = $this->login . '_climatic';
        @$sql = $this->db->select($table, array(), array());
        @$sth = $this->db->create($this->login . '_number_of_people', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'id_booking INT(11) NOT NULL'), 'id');
        while ($row = $sql->fetch()) {
            $sth = $this->db->add($this->login . '_number_of_people', $row['type']);
        }

        //create table for climatic payment
        @$table = $this->login . '_climatic';
        @$sql = $this->db->select($table, array(), array());
        @$sth = $this->db->create($this->login . '_climatic_payment', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'id_booking INT(11) NOT NULL'), 'id');
        while ($row = $sql->fetch()) {
            $sth = $this->db->add($this->login . '_climatic_payment', $row['type']);
        }
    }

}
