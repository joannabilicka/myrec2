<?php

class Dashboard_Model extends Model {

    function __construct() {
        parent::__construct();
        if (Session::get('loggedIn') == TRUE) {
            $login = Session::get('login');
            $this->login = $login;
        }
    }

    function reception() {
        
    }

    function start() {
        $link = mysqli_connect("localhost", "root", "", "myrec");
        $this->msg = '';
        $countBooking = 0;
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $phone = $_POST['phone'];
        $since = $_POST['since'];
        $until = $_POST['until'];
        $rtype = $_POST['rtype'];
        $rnumber = $_POST['rnumber'];

        if ($firstname != '' && $lastname != '' && $phone != '' && $since != '' && $until != '' && $rtype != '' && $rnumber != '') {
            //check if this dates and house are free to go
            $id_room_sql = "SELECT `id` FROM `" . $this->login . "_rooms`WHERE `type` ='" . $rtype . "'";
            $id_room = mysqli_query($link, $id_room_sql);
            $row_room = $id_room->fetch_assoc();

            $id_booking_sql = "SELECT since, until FROM " . $this->login . "_booking WHERE "
                    . "id_room = " . $row_room['id'] . " AND room_number=" . $rnumber . "  LIMIT 1";
            $id_booking = mysqli_query($link, $id_booking_sql);

            while ($row_booking = $id_booking->fetch_assoc()) {
                if (!(($row_booking['since'] > $since && $row_booking['since'] >= $until) ||
                        ($row_booking['until'] <= $since && $row_booking['until'] < $until))) {
                    $countBooking = 1;
                }
            }

            //check if this person is already in base
            $sth = $this->db->prepare("SELECT id FROM " . $this->login . "_guests WHERE "
                    . "firstname=:firstname AND lastname = :lastname  LIMIT 1");
            $sth->execute(array(
                ':firstname' => $_POST['firstname'],
                ':lastname' => $_POST['lastname']
            ));
            $countGuest = $sth->rowCount();

            //check if the period and house are free. If they are you can insert new booking
            //check if guest already exists in db. If he is, you dont insert it again. 
            if ($countBooking == 0) {
                if ($countGuest != 0) {
                    $this->insertBooking($link, $firstname, $lastname, $since, $until, $rtype, $rnumber);
                    $this->msg = 'Dodano rezerwacje na nazwisko ' . $firstname . ' ' . $lastname . ' ' . $phone . '<br> '
                            . 'na okres ' . $since . ' - ' . $until . ' na domku/pokoju ' . $rtype . ' ' . $rnumber;
                } else {
                    $this->insertGuest($firstname, $lastname, $phone);
                    $this->insertBooking($link, $firstname, $lastname, $since, $until, $rtype, $rnumber);
                    $this->msg = 'Dodano rezerwacje na nazwisko ' . $firstname . ' ' . $lastname . ' ' . $phone . '<br> '
                            . 'na okres ' . $since . ' - ' . $until . ' na domku/pokoju ' . $rtype . ' ' . $rnumber;
                }
            } else {
                $this->msg = 'Room/ House number ' . $rtype . ' ' . $rnumber . ' in period since ' . $since . ' until ' . $until . ' is alredy taken.<br> '
                        . 'Please choose other period or room/house. ';
            }
        } else {
            $this->msg = 'You have to insert all datas';
        }
    }

    //insert new booking
    function insertBooking($link, $firstname, $lastname, $since, $until, $rtype, $rnumber) {
        $id_room_sql = "SELECT `id` FROM `" . $this->login . "_rooms`WHERE `type` ='" . $rtype . "'";
        $id_room = mysqli_query($link, $id_room_sql);
        $row_room = $id_room->fetch_assoc();

        $id_guest_sql = "SELECT `id` FROM `" . $this->login . "_guests` WHERE `firstname` ='" . $firstname . "' AND `lastname` ='" . $lastname . "'";
        $id_guest = mysqli_query($link, $id_guest_sql);
        $row_guest = $id_guest->fetch_assoc();

        $sth = $this->db->prepare("INSERT INTO " . $this->login . "_booking(`id_guest`, `id_room`, `since`, `until`, `room_number`) "
                . "VALUES (:id_guest, :id_room, :since, :until, :room_number)");
        $sth->execute(array(
            'id_guest' => $row_guest['id'],
            ':id_room' => $row_room['id'],
            ':since' => $since,
            ':until' => $until,
            ':room_number' => $rnumber
        ));
    }

    //inserts new guest to the table
    function insertGuest($firstname, $lastname, $phone) {
        $sth = $this->db->prepare("INSERT INTO " . $this->login . "_guests( `firstname`, `lastname`, `phoneNumber`) "
                . "VALUES (:firstname, :lastname, :phoneNumber)");
        $sth->execute(array(
            ':firstname' => $firstname,
            ':lastname' => $lastname,
            ':phoneNumber' => $phone
        ));
    }

}
