<?php

class Login_Model extends Model {

    function __construct() {
        parent::__construct();
    }

    public function start() {
        $user = new User($_POST['login'], $_POST['password'], $_POST['password_again'], $_POST['email']);
        $sth = $this->db-> select('users', array ('login', '=', ':login', 'password', '=', ':passwprd' ), 
                array($user->login, sha1($user->password)));
        $count = $sth->rowCount();
        if ($count > 0) {
            // login
            Session::init();
            Session::set('loggedIn', true);
            Session::set('login', $user->login);
            Session::set('password', $user->password);
            Session::set('email', $user->email);
        }
    }

}
