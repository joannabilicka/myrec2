<?php

class Register_Model extends Model {

    function __construct() {
        parent::__construct();
        $sth = $this->db->create('users', array(
        'id INT(11) NOT NULL AUTO_INCREMENT',
        'login VARCHAR(20) NOT NULL',
        'password VARCHAR(64) NOT NULL',
        'email VARCHAR(30) NOT NULL'), 'id');
        }

        public function check() {
        global $user;
        $user = new User($_POST['login'], $_POST['password'], $_POST['password_again'], $_POST['email']);
        $this->msg = '';

        $sth = $this->db->select('users', array('login', '=', ':login'), array($user->getLogin()));
        $countLogin = $sth->rowCount();

        $sth = $this->db->select('users', array('email', '=', ':email'), array($user->getEmail()));
        $countEmail = $sth->rowCount();

        if (strlen($user->getLogin()) < 6 || strlen($user->getLogin()) > 20) {
        $this->msg = 'login musi miec dlugosc od 6 do 20 znakow';
        } else if ($countLogin != 0) {
            $this->msg = 'login zajęty. ';
        } else if ($user->getPassword() == '' || $user->getPasswordAgain() == '') {
            $this->msg = 'hasla nie moga byc puste';
        } else if ($user->getPassword() != $user->getPasswordAgain()) {
            $this->msg = 'hasla musza pasowac';
        } else if (strlen($user->getPassword()) < 6) {
            $this->msg = 'hasla musi miec conajmnie 6 znakow';
        } else if ($user->getEmail() == '') {
            $this->msg = 'podaj poprawny email';
        } else if ($countEmail != 0) {
            $this->msg = 'ten adres emai jest juz zajęty';
        } else {
            $this->start($user);
        }

        if (6 <= strlen($user->getLogin()) && strlen($user->getLogin()) <= 20 && $countLogin == 0) {
            $this->login = $user->getLogin();
        }

        if ($user->getEmail() != '' && $countEmail == 0) {
            $this->email = $user->getEmail();
        }
    }

    public function start($user) {
        $sth = $this->db->insert('users', array('login', 'password', 'email'));
        $sth->execute(array(
            ':login' => $user->getLogin(),
            ':password' => $user->getPassword(),
            ':email' => $user->getEmail()
        ));
        $this->make_tables($user);

        Session::init();
        Session::set('login', $user->getLogin());
        Session::set('reg', TRUE);
        header('location: ../config');
    }

    private function make_tables($user) {
        //create rooms/houses table for individual user
        $sth = $this->db->create($user->getLogin() . '_rooms', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'type VARCHAR (20) NOT NULL',
            'price INT(3) NOT NULL',
            'numberOf INT(4) NOT NULL'), 'id');
        //create meals table for individual user
        $sth = $this->db->create($user->getLogin() . '_meals', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'type VARCHAR (20) NOT NULL',
            'price INT(3) NOT NULL',
            'halfPrice INT(3)'), 'id');
        //create climatic charge table for individual user
        $sth = $this->db->create($user->getLogin() . '_climatic', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'type VARCHAR (20) NOT NULL',
            'price INT(3) NOT NULL'), 'id');
        //create other charge table for individual user
        $sth = $this->db->create($user->getLogin() . '_other', array(
            'id INT(11) NOT NULL AUTO_INCREMENT',
            'type VARCHAR (20) NOT NULL',
            'price INT(3) NOT NULL'), 'id');
    }

}
